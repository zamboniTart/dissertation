import argparse
from transformers import pipeline

def main():
    parser = argparse.ArgumentParser(
        prog='python cmp6200.py',
        description="This program identifies authors of a text using BERT Based Models",
        epilog="Made by Tomas Zakrocki"
    )

    # Arguments
    parser.add_argument(
        "string",
        action="store",
        help="Provide a string to be identified"
    )
    parser.add_argument(
        "-m", 
        "--model", 
        action="store",
        default="distilbert", 
        choices=['bert', 'distilbert'], 
        help="Choose the model you'd like to use (default is DistilBERT)"
    )
    parser.add_argument(
        "-r",
        "--raw",
        action="store_true",
        help="Outputs raw output from the model"
    )
    parser.add_argument(
        "-a",
        "--author",
        action="store_true",
        help="Only print the author of the string"
    )
    # Parse all arguments that have been entered
    args = parser.parse_args()
    
    # Pick Models
    if args.model.lower() == "bert":
        model = "./model/BERT"
    elif args.model.lower() == "distilbert":
        model = "./model/distilBERT"
    else:
        exit()

    # Actual code for classification
    classifier = pipeline("text-classification", model=model)
    result = classifier(args.string)
    unpacked = result[0]
    author = unpacked["label"].lower().capitalize()
    percent = unpacked["score"] * 100


    # Terminal Output
    if args.raw == True:
        print(unpacked)
    elif args.author == True:
        print(author)
    else:
        print("This text was written by {}, with a {:0.2f}% certainty.".format(author, percent))

main()