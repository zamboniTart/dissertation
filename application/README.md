# CMP6200 Authorship Attribution Program
Tomas Zakrocki - 21183586

## How to run
There are two ways to run this program:
- Natively (using `pip` and `venv`)
- Using the provided `Dockerfile`

### Docker
With docker installed on your system:
1. Build the image using the provided `Dockerfile`: `docker built -t cmp6200 .`
2. Run the program with `docker run cmp6200 python cmp6200.py {options} {string}`

### Natively
With python installed on your system:
1. Create the venv, `python -m venv venv`
2. Enter the venv:
   - Windows: `venv\Scripts\activate.bat` or `venv\Scripts\Activate.ps1` in Powershell
   - Linux/MacOS: `source venv/bin/activate`
3. Install dependencies:
   - PyTorch: `pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu`
   - Transformers: `pip install transformers`
4. Run the program!

## Usage
For help, run `python cmp6200.py --help`

## Miscellaneous
Tested with PyTorch CPU rendering on an i7-12700 and i5-1240P.