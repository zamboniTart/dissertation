import evaluate
from evaluate import TextClassificationEvaluator, evaluator
from datasets import Dataset
from transformers import pipeline, AutoTokenizer
import pandas as pd

model_choice = input("Which model would you like to choose? (1: BERT, 2: DistilBERT) ")

if model_choice == "1":
    model = "../application/model/BERT"
    tokenizer = AutoTokenizer.from_pretrained("google-bert/bert-base-cased")
elif model_choice == "2":
    model = "../application/model/distilBERT"
    tokenizer = AutoTokenizer.from_pretrained("distilbert/distilbert-base-cased")
else:
    exit()

initial_dataset = pd.read_csv("test_split.csv")
initial_dataset.dropna(axis=0, inplace=True)

dataset = Dataset.from_pandas(initial_dataset)
task_evaluator = evaluator("text-classification")
classifier = pipeline(task="text-classification", model=model, max_length=512)

eval_results = task_evaluator.compute(
    model_or_pipeline=classifier,
    data=dataset,
    input_column="text",
    label_column="labels",
    label_mapping={"TOMAS": 0, "YIFF": 1},
    metric=evaluate.combine(["accuracy","confusion_matrix", "ealvaradob/false_positive_rate"])
)
print(eval_results)
