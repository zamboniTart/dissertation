# Dissertation - Machine Learning methods to detect an author

The idea behind this project is to be able to determine whether the author of a message (or email) is genuine using machine learning methods.

In the event that an attacker gains access to an email that has privileged access, there are few protections to verify whether an email is being written by the owner of the address, or another party. This mitigates that issue by training a machine learning model based on a database of personal emails, and giving a score as to how genuine an email is. 

## Development Environment
I have an Arch Linux Distrobox container, running the appropriate ROCm and HIP libraries necessary for Compute on my AMD GPU (6700XT), within that container is a python venv that contains all the python libraries necessary for development. 

# Need to know
The training/data directory has been added to `.gitignore` for privacy reasons.

# Todo: 
- [ ] Create script to setup environment (Optional, just make things easier)
- [X] Dataset
  - [x] Create initial dataset
  - [x] Process dataset
    - [x] User IDs
    - [x] Custom Emoji
    - [x] URLs
    - [x] Bot Comamnds
    - [x] Channels
    - [x] Roles
    - [x] Tags (@everyone, @here etc.)
    - [x] IP addresses
  - [X] Gather other datasets of messages from different authors
  - [X] Add them all (with labels) to one large dataset
- [X] Fine-tune BERT on this and create the model
