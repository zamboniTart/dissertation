- Research
	- [[Author Attribution]]
		- TODO Pre-existing author attribution using "old" machine learning techniques (such as SVM, Decision Trees etc) #[[ML Algorithm]]
		  :LOGBOOK:
		  CLOCK: [2023-12-13 Wed 13:45:33]--[2023-12-13 Wed 13:45:34] =>  00:00:01
		  CLOCK: [2023-12-13 Wed 13:45:37]--[2023-12-13 Wed 13:45:37] =>  00:00:00
		  :END:
			- I already know a fair bit from CMP6328 but I need to do more about specifically detecting an author of a message (or literature).
		- DOING Research into author attribution using [[Deep Learning]] techniques and effectiveness
		  :LOGBOOK:
		  CLOCK: [2023-12-18 Mon 14:14:38]
		  :END:
	- [[Deep Learning]]
		- Best practises with training, fine-tuning, dataset pre-processing etc. #Dataset #Finetune #Training
		- #[[Dev Research]]
	- [[Articles]]
- Plan
	- Model Selection #[[Specific Model]]
	- [[Dataset]]
- Practical
	- [[Coding]]
		- Sample code has been written and tested with GPU acceleration with pytorch, all working!
		- Read into [hugging face documentation](https://huggingface.co/docs) #[[Dev Research]]
			- TODO For [[Metrics]] [huggingface/evaluate](https://huggingface.co/docs/evaluate/index) to find out how to judge the model etc.
			- TODO Pick [[Specific Model]] for usage
			- TODO How to [[Finetune]] model
		- Prerequisites for the program #[[Deep Learning]]
			- TODO Prepare [[Dataset]] by pruning personal information, bad entries etc.
				- Should be fine, there have been #[[ML Algorithm]] methods that have worked on separated messages rather than long emails, I'm hoping that the fact that I'm using more detailed data instances + more modern [[Deep Learning]] techniques will make this work better
			- TODO Figure out which model to use for the program, and how to compare text to an input #[[Specific Model]]
	- [[Development Environment]]
		- Development environment has been setup, but it's messy and not portable.
		- The development environment uses a distrobox container (with the docker backend) for an Arch Linux container that runs all necessary packages and libraries for running the code. #[[Dev Info]]
		- Nix Flakes were unable to be used due to Nix's way of sandboxing packages and using non-standard file paths for everything #[[Dev Info]]
		- TODO Setup ``flake.nix`` for automatic deployment of environment
		- TODO Ensure that ``HSA_OVERRIDE_GFX_VERSION=10.3.0`` is enabled and global
		- TODO Enter ``venv`` automatically
- Deadlines
	- [[A2]] - 22nd January 2024