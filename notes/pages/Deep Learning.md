- Research
	- What is [[BERT]]? [Article](https://towardsdatascience.com/keeping-up-with-the-berts-5b7beb92766)
		- BERT stands for Bidirectional Encoder Respresentations from Transoformers
		- It's designed as a Base Model that is pretrained on many tasks, and then fine-tuned to doing those tasks. Rather than the classic way of doing things where you would train a model on one specific task, and then fine-tune it further to make it better at that specific task.
		- The way that most language models are trained are with unrelated tasks, these tasks help develop a contextual understanding of words in a model. One model is unable to be used bi-directionally because it would see the same sentence from the opposite direction which is why most frameworks use two instances of a model (such as [[RNN]])
		- The way that BERT is able to be trained bi-directionally is a process known as [[Masked Language Model]] or MLM.
			- Some percentage of the input tokens are masked at random and the model tries to predict these tokens. These predicted tokens are fed into an output softmax to get the final output words.
				- An output softmax turns a vector of K real numbers into a probability distribution of K possible outcomes. This enables the model to be fine-tuned on many different things because it would output probability rather than a word.
			- Normally this wouldn't work because fine-tuning doesn't involve predicting masked words, how is BERT able to be fine-tuned to do things like classification if it was only trained on masked sentences?
				- The way BERT does this is by adjusting the MASK input tokens:
				  15% of the words are masked while training, but within those words not all are replaced with the [MASK] token.
				  80% are replaced with the [MASK] token.
				  10% are replaced with random tokens.
				  10% are unchanged input tokens that were being masked.
			- BERT is then further trained on [[Next Sentence Prediction]] (NSP).
				- The MASK token method doesn't account for other things though, there are other things that BERT is able to do such as Question Answering or Natural Language Inference that need another method of training to actually work. This is where NSP comes in.
				- The way this training works, it picks two sentences: A and B.
				  50% of the time B is the actual next sentence that follows A, and the other 50% it's a random sentence.
		- BERT is trained using both of these methods *at the same time*. The way this works is by putting two different instances of data within the same input. The authors note that a "sentence" doesn't need to be an actual sentence, but rather a span of contiguous text. A [SEP] token is used to separate them, as well as seperating the sentences within [[Next Sentence Prediction]] .
			- ![](https://miro.medium.com/v2/resize:fit:700/1*_d5e72O312RkQkasUJ3EnQ.png){:height 205, :width 666}
			- One problem with this is that the inputs are supposed to fed in one step, as opposed to RNNs (which is what BERT is using) where all inputs are fed sequentially and the model is unable to preserve the ordering of the input tokens.
			- Another problem is that NSP requires a distinction between sentence A and B. It's unable to do this if everything is sequential.
			- Both issues are resolved by adding [[embeddings]] to the tokens and using the results as the inputs. Kind of like metadata(?)
				- Segment Embeddings provide information about the sentence that a token is part of
				- Positive Embeddings provide information about the order of the words in the input.
		-
	- What is a Bidirectional Model? [Article](https://medium.com/@plusepsilon/the-bidirectional-language-model-1f3961d1fb27)
	  collapsed:: true
		- The idea behind language models is to predict words in a sentence/phrase when only supplied with certain other words. For example if supply a model with the word "you" it can predict either a left context or a right context.
			- A model that's trained in right context might predict "you are okay", while a model trained in left context would predict "how are you".
			- A Bidirectional model is able to use *both* right and left contexts to see how a word fits into a sentence.
		- Most of the time this is done with two separate [[RNN]] hidden vectors, where one is a Forward RNN (left context) and the other is a Reverse RNN (right context) and most deep learning frameworks return this as two separate sets.