import pandas as pd
import re
import datasets as ds
from datasets import Dataset
from datasets import ClassLabel

personal_dataset = pd.read_parquet('data/all/personal.parquet')
chirunder_dataset = pd.read_parquet('data/all/chirunder-textmessages-1.parquet')
yiff_dataset = pd.read_parquet('data/all/yiff-discord.parquet')

# Rename column (to stay consistent with the other one)
personal_dataset.rename(columns={'Contents': 'text'}, inplace=True)
# Insert author column with the author's name
personal_dataset.insert(1, "labels", "tomas")
chirunder_dataset.insert(1, "labels", "chirunder")
yiff_dataset.insert(1, "labels", "yiff")

# Making sure everything's been removed etc.
personal_dataset.drop_duplicates(inplace=True)
personal_dataset.dropna(axis=0, inplace=True)
yiff_dataset.drop_duplicates(inplace=True)
yiff_dataset.dropna(axis=0, inplace=True)

# Convert to Hugging Face format
hg_personal_dataset = Dataset.from_pandas(personal_dataset)
hg_chirunder_dataset = Dataset.from_pandas(chirunder_dataset)
hg_yiff_dataset = Dataset.from_pandas(yiff_dataset)

unmapped_dataset = ds.concatenate_datasets([hg_personal_dataset.select(range(3500)), hg_chirunder_dataset.select(range(3500)), hg_yiff_dataset.select(range(3500))])
experiment_2_dataset = unmapped_dataset.cast_column('labels', ClassLabel(names=["tomas","chirunder","yiff"]))

experiment_2_dataset.to_parquet("data/all/experiment_2.parquet")