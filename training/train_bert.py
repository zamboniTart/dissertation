import os
import numpy as np
import pandas as pd
import evaluate
from transformers import TrainingArguments, Trainer
from transformers import TrainingArguments
from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification
from datasets import load_dataset, Dataset, ClassLabel

# Heavily based on documentation for fine-tuning a pretrained model: https://huggingface.co/docs/transformers/v4.39.1/en/training

def tokenize_function(examples):
    return tokenizer(examples["text"], padding="max_length", truncation=True, max_length=512)

def compute_metrics(eval_pred):
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    return metric.compute(predictions=predictions, references=labels)

model_choice = input("Which model would you like to use? (1 for BERT, 2 for DistilBERT): ")
if model_choice == "1":
    chosen_model = "google-bert/bert-base-cased"
    output_dir = "../models/BERT/"
elif model_choice == "2":
    chosen_model = "distilbert/distilbert-base-cased"
    output_dir = "../models/DistilBERT/"
else:
    exit()

dataset_choice = input("Which Dataset would you like to use? (1,2,3,4): ")
if dataset_choice == "1": 
    chosen_dataset = "data/all/experiment_1.parquet"
    labels = 2
    id2label = {0: "TOMAS", 1:"CHIRUNDER"}
    label2id = {"TOMAS": 0, "CHIRUNDER":1}
elif dataset_choice == "2":
    chosen_dataset = "data/all/experiment_2.parquet"
    labels = 3
    id2label = {0: "TOMAS", 1:"CHIRUNDER", 2:"YIFF"}
    label2id = {"TOMAS": 0, "CHIRUNDER":1, "YIFF":2}
elif dataset_choice == "3":
    chosen_dataset = "data/all/experiment_3.parquet"
    labels = 2
    id2label = {0: "TOMAS", 1:"YIFF"}
    label2id = {"TOMAS": 0, "YIFF":1}
elif dataset_choice == "4":
    chosen_dataset = "data/all/experiment_3_duplicates.parquet"
    labels = 2 
    id2label = {0: "TOMAS", 1:"YIFF"}
    label2id = {"TOMAS": 0, "YIFF":1}
else: 
    exit()

counter = 1
while True:
    folder_name = output_dir + str(counter)
    if os.path.exists(folder_name):
        counter += 1
        print("Folder exists")
    else:
        print("Folder doesn't exist")
        break

# Verify that empty rows are removed.
tokenizer = AutoTokenizer.from_pretrained(chosen_model)
model = AutoModelForSequenceClassification.from_pretrained(chosen_model, num_labels=labels, id2label=id2label, label2id=label2id)

initial_dataset = load_dataset("parquet", data_files=chosen_dataset, split="train")
dataset = initial_dataset.train_test_split(test_size=0.2, stratify_by_column="labels", seed=42)
tokenized_dataset = dataset.map(tokenize_function, batched=True)
dataset["test"].to_csv("test_split.csv")

training_args = TrainingArguments(output_dir=folder_name, evaluation_strategy="epoch")
metric = evaluate.load("accuracy")

trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_dataset["train"],
    eval_dataset=tokenized_dataset["test"],
    compute_metrics=compute_metrics
)
trainer.train()
trainer.save_model(folder_name + "/dataset" + dataset_choice)
