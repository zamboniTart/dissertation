import pandas as pd
import re
import numpy as np

# Function to remove patterns from dataset. Depending on `drop` it will replace with '' or drop the row.
def remove_pattern(dataset, pattern, drop):
    for index, row in dataset.itertuples():
        match = re.search(pattern, str(row))
        if match and drop == False:
            dataset.iat[index, 0] = re.sub(pattern, '', str(row))
        # For certain things (like bot commands) you want to just drop the entire entry, rather than replace it.
        if match and drop == True:
            dataset.drop(index=index, inplace=True)

path = "data/all/unprocessed/"

filename = input("What is the filename? (including extension) ")
dataset = pd.read_parquet(path + filename)
print("Reading Dataset!")

intial_size = len(dataset.index)
print("Dataset is {} rows!".format(intial_size))

# Regex for different things I want to remove
url_pattern = r"https?://\S+"
userID_pattern = r"<@!?[0-9]{17,19}>"
role_pattern = r"<@&[0-9]{17,21}>"
channel_pattern = r"<#[0-9]{17,21}>"
emoji_pattern = r"<a?:[a-zA-Z0-9_]+:[0-9]{17,21}>"
ip_pattern =  r"[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
tag_pattern = r"@[a-zA-Z]{1,}"
bot_commands_pattern = r"^[+!?\-%.\/>][a-zA-Z]{1,}"

print("Removing dataset entries with regex patterns...")
remove_pattern(dataset, url_pattern, False)
remove_pattern(dataset, userID_pattern, False)
remove_pattern(dataset, role_pattern, False)
remove_pattern(dataset, channel_pattern, False)
remove_pattern(dataset, emoji_pattern, False)
remove_pattern(dataset, ip_pattern, False)
remove_pattern(dataset, tag_pattern, False)
remove_pattern(dataset, bot_commands_pattern, True)

print("Dropping duplicates and empty entries")
dataset.drop_duplicates(inplace=True)
dataset.dropna(axis=0, inplace=True)

new_size = len(dataset.index)

print("{} rows removed, new dataset size is {} rows (from {})".format(intial_size - new_size, new_size, intial_size))

dataset.to_parquet("data/all/" + filename, index=False)